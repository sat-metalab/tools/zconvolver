 # Ambisonic vonvolution reverb

General pipeline to play mono files through ambisonic reverb, re-encoded to binaural:

```
audiofile.wav --> jconvolver --> SATIE
```

Dependencies:
- jconvolver
- SATIE

Steps to achieve this:

- decide which order you need to play and provide appropriate IR (a multichannel `.wav` file recorded at 48000Hz sampling rate)
- according to the chosen order use corresponding script:
  - 2nd order [jc\_ambi2\_start.sh](jc_ambi2_start.sh)
  - 3rd order [jc\_ambi3\_start.sh](jc_ambi3_start.sh)
- start jack
- open a terminal and execute first:
  - 2nd order: `sclang ambi2_to_binaural.scd`
  - 3rd order: `sclang ambi2_to_binaural.scd`
  Some windows will pop-up, namely a VU-meter, a stethoscope, a UI for controling overall playback volume (and some info)
- and then: `./jc_xxxx.sh <absolute/path/to/IR_file>.wav`
- watch out for any errors reported (normally it will echo the config to terminal and may report a harmless message of this kind `Line 2: partition size adjusted to 4096.`)
- observe that supernova has created appropriate number of input channels
- open another terminal (or another window in `terminator` or `byobu`)
- play a mono file and connect it to the `jconvolver:input` port in jack:
  i.e. `mplayer -ao jack:port=jconvolver snd/floors.wav`

If you want to record the result, see the server GUI and use the record button. Watch the terminal for information on its name and location (usually in `~/.local/share/SuperCollider/Recordings`).

Note: First run of `.sh` script will generate a jconvolver configuration called `cur_ir.conf` in the current directory. Each successive run will simply replace it.
