#!/bin/bash

IRFILE=$1
IRDIR=$(dirname "${IRFILE}")
HEADER="/cd $IRDIR"
#                       in  out   partition    maxsize    density
#       --------------------------------------------------------
JACKIO="/convolver/new   1  9     256           204800      1.0\n
#              num   port name     connect to
# ----------------------------------------------
/input/name     1     input

/output/name    1     Out.W          supernova:input_1
/output/name    2     Out.X          supernova:input_2
/output/name    3     Out.Y          supernova:input_3
/output/name    4     Out.Z          supernova:input_4
/output/name    5     Out.R          supernova:input_5
/output/name    6     Out.S          supernova:input_6
/output/name    7     Out.T          supernova:input_7
/output/name    8     Out.U          supernova:input_8
/output/name    9     Out.V          supernova:input_9
"

IR="\n
#               in out  gain  delay  offset  length  chan      file
# ---------------------------------------------------------------------
/impulse/read   1   1   1   0     0        0       1        $1
/impulse/read   1   2   1   0     0        0       2        $1
/impulse/read   1   3   1   0     0        0       3        $1
/impulse/read   1   4   1   0     0        0       4        $1
/impulse/read   1   5   1   0     0        0       5        $1
/impulse/read   1   6   1   0     0        0       6        $1
/impulse/read   1   7   1   0     0        0       7        $1
/impulse/read   1   8   1   0     0        0       8        $1
/impulse/read   1   9   1   0     0        0       9        $1
"

echo -e "$HEADER \n$JACKIO \n$IR" > cur_ir.conf
# cat cur_ir.conf
jconvolver -s default cur_ir.conf
